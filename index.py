from flask import Flask, render_template, request, redirect, url_for, flash, session

import psycopg2
import bcrypt

conect = psycopg2.connect(
    dbname='proyectoPython',
    user='leoalvarez',
    password='leo444dev',
    host='localhost',
    port='5432'
)
cursor= conect.cursor()
#encript
semilla = bcrypt.gensalt()
# seting

app = Flask(__name__)

app.secret_key = 'mysecretkey'

@app.route('/')
def home():
    return render_template('frontend/home.html')

@app.route('/AboutMe')
def about():
    return render_template('frontend/about.html')

@app.route('/Contact')
def contact():
    return render_template('frontend/contact.html')

@app.route('/MisTrabajos')
def works():
    return render_template('frontend/portfolio.html')


@app.route('/sendForm', methods=['POST'])
def sendForm():
    if request.method == 'POST':
        email = request.form['email']
        Name = request.form['name']
        LastName = request.form['LastName']
        message = request.form['message']
        sql ="INSERT INTO formcontact (Nombre, Apellido, email, message) VALUES (%s,%s,%s,%s)"
        cursor.execute(sql,(Name,LastName,email,message))
        conect.commit()
        flash('consulta enviada.')
        return redirect(url_for('home'))

if __name__ == '__main__':
    app.run(debug=True)
